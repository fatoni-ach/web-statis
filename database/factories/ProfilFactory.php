<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profil;
use Faker\Generator as Faker;

$factory->define(Profil::class, function (Faker $faker) {
    return [
        'nama_lengkap' => $faker->unique()->name(),
        'email'         => $faker->unique()->email(),
        'foto'          => $faker->text($maxNbChars = 40),
    ];
});
