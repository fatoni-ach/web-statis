<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar_jawaban extends Model
{
    public function jawaban()
    {
        return $this->belongsto('App\Jawaban');
    }
    public function profil()
    {
        return $this->belongsto('App\Profil');
    }
}
