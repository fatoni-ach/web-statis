<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class like_dislike_pertanyaan extends Model
{
    public function pertanyaan()
    {
        return $this->belongstomany('App\Pertanyaan');
    }
    public function profil()
    {
        return $this->belongstomany('App\Profil');   
    }
}
