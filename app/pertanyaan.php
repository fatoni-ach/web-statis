<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    protected $fillable = ['judul', 'isi', 'jawaban_tepat_id', 'profil_id'];
    // use SoftDeletes;

    public function profil()
    {
        return $this->belongsto('App\Profil');
    }

    public function komentar_pertanyaans()
    {
        return $this->hasmany('App\komentar_pertanyaan');
    }
    public function jawabans()
    {
        return $this->hasmany('App\Jawaban');
    }
    public function jawaban_tepat()
    {
        return $this->belongsto('App\Jawaban', 'jawaban_id');
    }
    public function like_dislike_pertanyaans()
    {
        return $this->belongstomany('App\Profil', 'App\like_dislike_pertanyaan', 'pertanyaan_id', 'profil_id');
    }

}
