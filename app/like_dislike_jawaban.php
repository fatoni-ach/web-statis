<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class like_dislike_jawaban extends Model
{
    public function profil()
    {
        return $this->belongstomany('App\Profil');
    }
    public function jawaban()
    {
        return $this->belongstomany('App\Jawaban');
    }
}
