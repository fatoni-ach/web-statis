<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jawaban extends Model
{
    public function pertanyaan()
    {
        return $this->belongsto('App\Pertanyaan');
    }
    public function komentar_jawabans()
    {
        return $this->hasmany('App\Komentar_jawaban');
    }
    public function profil()
    {
        return $this->belongsto('App\Profil');
    }
    public function like_dislike_jawabans(Type $var = null)
    {
        return $this->belongstomany('App\profil', 'App\like_dislike_jawaban', 'jawaban_id', 'profil_id');
    }

}
