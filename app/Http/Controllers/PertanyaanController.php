<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;
use App\{Pertanyaan, Profil};

class PertanyaanController extends Controller
{
    public function index()
    {

        $pertanyaan = Pertanyaan::get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {   
        if($request->method() == "POST"){
            $request = $request->validate([
                'judul' => 'required|unique:pertanyaans|max:45',
                'isi' => 'required',
            ]);
            $pertanyaan = Pertanyaan::create([
                'judul' => $request['judul'], 
                'isi' => $request['isi']
            ]);
        }
        return redirect('/pertanyaan');
    }

    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.update', compact('pertanyaan'));
    }
    public function update($id, Request $request)
    {
        if($request->method() == "PUT"){
            $request = $request->validate([
                'judul' => 'required|unique:pertanyaans|max:45',
                'isi' => 'required',
            ]);
            $pertanyaan = Pertanyaan::find($id);
            $pertanyaan->judul = $request['judul'];
            $pertanyaan->isi = $request['isi'];
            $pertanyaan->save();
        }
        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        if (request()->method() == "DELETE"){
            Pertanyaan::find($id)->delete();
        }
        return redirect('/pertanyaan');
    }
}
