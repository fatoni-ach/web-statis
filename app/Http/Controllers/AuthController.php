<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function register()
    {
        return view('form');
    }
    public function logout_admin()
    {
        Auth::logout();
        return redirect('/');
    }
}
