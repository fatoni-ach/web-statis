<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    //

    public function pertanyaans()
    {
        return $this->hasmany('App\Pertanyaan');
    }
    public function komentar_pertanyaans()
    {
        return $this->hasmany('App\Komentar_pertanyaan');
    }
    public function like_dislike_pertanyaans()
    {
        return $this->belongstomany('App\Pertanyaan', 'App\like_dislike_pertanyaan', 'profil_id', 'pertanyaan_id');
    }
}
