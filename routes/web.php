<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'AuthController@index')->name('index');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/welcome', 'HomeController@welcome')->name('welcome');
Route::get('/data-table', function ()
{
    return view('table.data-table');
})->name('data-tables');
Route::group(['middleware' => 'auth'], function () {
    Route::resource('/pertanyaan', 'PertanyaanController')->except([
        'index', 'show'
    ]);
    Route::get('/logout-admin', 'AuthController@logout_admin')->name('logout_admin');
});
Route::resource('/pertanyaan', 'PertanyaanController')->only([
    'index', 'show'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
