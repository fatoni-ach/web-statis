@extends('layouts.master')

@section('title')
    Pertanyaan
@endsection

@section('body')
<div class="card">
<div class="card-header">
    <h3 class="card-title">Pertanyaan</h3>
    <div class="card-tools">
    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fas fa-minus"></i></button>
    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fas fa-times"></i></button>
    </div>
</div>
<div class="card-body">
    <form class="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
        @csrf
        @method("PUT")
        <label for="judul">Judul Pertanyaan</label>
        <input class="form-control" type="text" id="judul" value="{{ $pertanyaan->judul ?? '' }}" name="judul">
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="isi">Isi Pertanyaan</label>
        <textarea class="form-control" type="text" id="isi" name="isi" rows="4">{{  $pertanyaan->isi ?? '' }}</textarea><br>
        @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
    </form>    
</div>
</div>
    
@endsection
    
