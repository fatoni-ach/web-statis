@extends('layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('title')
    Pertanyaan
@endsection

@section('body')
<div class="card">
<div class="card-header">
    <h3 class="card-title">Pertanyaan</h3>
    <div class="card-tools">
    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fas fa-minus"></i></button>
    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fas fa-times"></i></button>
    </div>
</div>
<div class="card-body">
    <a class="btn btn-primary mb-2" href="/pertanyaan/create"><i class="fa fa-plus" aria-hidden="true"></i>Tambah</a>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>No</th>
            <th>Judul Pertanyaan</th>
            <th>Isi Pertanyaan</th>
            <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($pertanyaan as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->judul}}</td>
                <td>{{$value->isi}}</td>
                <td><form action="pertanyaan/{{$value->id}}" method="post">
                        <a class="btn btn-sm btn-info" href="pertanyaan/{{$value->id}}">Show</a>
                        <a class="btn btn-sm btn-warning" href="pertanyaan/{{$value->id}}/edit">Edit</a>
                        @csrf
                        @method("DELETE")
                        <button class="btn btn-sm btn-danger" role="button" type="submit">Hapus</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">
                    Belum ada data
                </td>
            </tr>
                
            @endforelse
        </tbody>
        </table>
</div>
</div>
    
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }} "></script>
    <script>
        $(function () {
        $("#example1").DataTable();
        });
    </script>
@endpush

    
