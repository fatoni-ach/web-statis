@extends('layouts.master')

@section('title')
    Sign Up Form
@endsection

@section('body')
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Buat Account Baru!</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <form class="form-group" action="{{Route('welcome')}}" method="POST">
                @csrf
                <label for="firstName">First name : </label>
                <input class="form-control form-control-sm" type="text" name="firstName" id="firstName">
                <label for="lastName">Last name : </label>
                <input class="form-control form-control-sm" type="text" name="lastName" id="lastName">
                <label for="gender">Gender : </label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="male" value="Male">
                    <label class="form-check-label" for="male">Male</label><br>
                    <input class="form-check-input" type="radio" name="gender" id="female" value="Male">
                    <label class="form-check-label" for="female">Female</label><br>
                    <input class="form-check-input" type="radio" name="gender" id="other" value="Other">
                    <label class="form-check-label" for="other">Other</label>
                </div>
                <label for="nationality">Nationality : </label><br>
                <select class="form-control" name="nationality" id="nationality">
                    <option value="1">Indonesia</option>
                    <option value="2">Singapore</option>
                    <option value="3">Malaysia</option>
                    <option value="4">Thailand</option>
                </select>
                <label for="language">Language Spoken : </label><br>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="language[]" value="Bahasa Indonesia" id="indonesia">
                    <label for="indonesia">Bahasa Indonesia</label>  <br>
                    <input class="form-check-input" type="checkbox" name="language[]" value="English" id="english">
                    <label for="english">English</label>  <br>
                    <input class="form-check-input" type="checkbox" name="language[]" value="Other" id="other1">
                    <label for="other1">Other</label><br>
                </div>
                <label for="bio">Bio : </label><br>
                <textarea class="form-control form-control-sm" name="bio" id="bio" cols="30" rows="10"></textarea><br>
                <button class="btn btn-primary btn-sm" type="submit">Sign Up</button>
            </form>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
    
@endsection

