@extends('layouts.master')

@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('body')
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Welcome</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <h1>SELAMAT DATANG {{$firstname . " " . $lastname}} !</h1>
            <h3>Terima Kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
    
@endsection

